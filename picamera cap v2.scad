/*
thickness to top of lens: 6.5mm
thickness to base of lens: 4
lens radius: 7
board thickness: 1mm
seattube angle=28 from vertical
seattube diameter=18.4
*/
$fn=100;
bolt_holes=[[0,0],[12.5,0],[12.5,21],[0,21]];
outer_box=[[-3,0],[60,0],[60,23],[-3,23]];
position=[20,-1,0];
angle=28;
    
difference() {
    union() {
        rotate([0,angle,0]) {
            intersection() {
                union() {
                    rotate([0,-angle,0]) translate([0,-50,0]) scale([ 1,1,1]) cube([100,100,2]);
                    rotate([0, angle,0]) translate([0,-50,0]) scale([-1,1,1]) cube([100,100,2]);
                }
                translate([-28.5,0,0]) linear_extrude(height=100) offset(6) polygon(outer_box);
            }
            difference() {
                translate([-28.5,0,0]) linear_extrude(height=24) offset(6) polygon(outer_box);
                rotate([0,-angle,0]) translate ([0,-50,0]) scale([ 1,1,-1]) cube([100,100,100]);
                rotate([0, angle,0]) translate ([0,-50,0]) scale([-1,1,-1]) cube([100,100,100]);
                translate([-28.5,0,0]) linear_extrude(height=100) offset(2) polygon(outer_box);
                translate([0,0,22]) difference() {
                    translate([-28.5,0,0]) linear_extrude(height=100) offset(6) polygon(outer_box);
                    translate([-28.5,0,0]) linear_extrude(height=100) offset(4) polygon(outer_box);
                }
            }
            translate([-10,29,22]) rotate([0,90,0]) linear_extrude(height=20) polygon([[0,0],[4,0],[6.6,2.6],[6.6,4.6],[0,4.6]]);
            translate([10,-6,22]) rotate([0,90,180]) linear_extrude(height=20) polygon([[0,0],[4,0],[6.6,2.6],[6.6,4.6],[0,4.6]]);
        }    
        translate(position) union() {
            translate([2,2,0]) cylinder(r=1.5,h=5.6);
            translate([14.5,2,0]) cylinder(r=1.5,h=5.6);
            translate([2,23,0]) cylinder(r=1.5,h=5.6);
            translate([14.5,23,0]) cylinder(r=1.5,h=5.6);
            translate([2,2,0]) cylinder(r=1,h=6.6);
            translate([14.5,2,0]) cylinder(r=1,h=6.6);
            translate([2,23,0]) cylinder(r=1,h=6.6);
            translate([14.5,23,0]) cylinder(r=1,h=6.6);

            translate ([8.25,0,0]) rotate([90,0,-90]) translate([0,0,-3]) linear_extrude(height=6) polygon([[1,0],[1,7.8],[0,7.8],[-.6,7.2],[1,5.6]]);
            
            translate ([8.25,25,0]) rotate([90,0,90]) translate([0,0,-3]) linear_extrude(height=6) polygon([[1,0],[1,7.8],[0,7.8],[-.6,7.2],[1,5.6]]);
        }
    }
    translate(position) translate([2.1,12.5,-1]) cylinder(r=4,h=100);
}