/*
lower leg inside spread 23;
lower leg diameter 17;
upper leg diameter 15
*/
$fn=100;
outer_box=[[-3,0],[60,0],[60,23],[-3,23]];

difference() {
    translate([9,-11.5,0]) {
        linear_extrude(height=2) offset(6) polygon(outer_box);
        difference() {
            linear_extrude(height=4) offset(4) polygon(outer_box);
            linear_extrude(height=4) offset(2) polygon(outer_box);
        }
        scale([1,1,-1]) linear_extrude(height=8) offset(6) polygon(outer_box);
    }
// NB: this almost certainly needs to be adjusted for your bicycle...especially the cylinder radii.
// the rotation accounts for the spread of the seatstays as they lead from the seatpost to the rear dropouts.
    translate([-2,11,-9]) rotate([0,90,8]) cylinder(r1=8,r2=9,h=77) ;
    translate([-2,-11,-9]) rotate([0,90,-8]) cylinder(r1=8,r2=9,h=77) ;
}
