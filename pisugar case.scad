$fn=100;

bolt_holes=[[0,0],[58,0],[58,23],[0,23]];
outer_box=[[-3,0],[60,0],[60,23],[-3,23]];

translate([10,-10,0]) union() {
    cube([5.5,4.5,1]);
    translate([0,1,0]) cube([5.5,2.5,4]);
}

translate([0,0,30]) rotate([180,0,0]) union() {
    difference() {
        linear_extrude(height=30) union() {
            difference() {
                offset(6) polygon(outer_box);
                offset(4) polygon(outer_box);
            }
        }
        translate([32.9,-5,17.2]) cube([10,10,5]);
        translate([45.5,-5,17.2]) cube([10,10,5]);
        translate([36,-7,11]) cube([9.5,10,5]);
        translate([47,-5,11]) cube([6,10,5]);
        translate([47,-7,12]) cube([6,10,3]);
        translate([0,5,17.2]) {
            for (y=[0:4:12]) {
                translate([0,y,0]) cube([100,1,1]);
            }
        }
    }
    translate([0,0,16]) linear_extrude(height=1.2) union() {
        difference() {
            polygon([[-7,10],[7,-4],[-7,-4]]);
            translate(bolt_holes[0]) scale([.5,.5]) circle(r=3);
        }
        difference() {
            polygon([[64,6],[64,-4],[46,-4]]);
            translate(bolt_holes[1]) scale([.5,.5]) circle(r=3);
        }
        difference() {
            polygon([[64,17],[64,27],[46,27]]);
            translate(bolt_holes[2]) scale([.5,.5]) circle(r=3);
        }
        difference() {
            polygon([[-7,13],[7,27],[-7,27]]);
            translate(bolt_holes[3]) scale([.5,.5]) circle(r=3);
        }
    }
}