This is a Raspberry Pi based camera intended for use on a bike.
It uses the rpi zero w paired with the pisugar
I get about 4 hours of recording with a 1200mAh battery, and the audio is recorded from my bluetooth headset.  

NB: the "pisugar base.scad" file should be edited to fit your bike, and the MAC address in the rpeye.py application should be set to match your headset...but it'll work without it.

The output video and audio are in separate files, but could obviously be joined...it's just more efficient to leave them separate...I figure you could join them in post processing.
