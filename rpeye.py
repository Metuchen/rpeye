#!/usr/bin/env python3

from picamera import PiCamera
from subprocess import Popen, run, PIPE, DEVNULL
from time import time,strftime
import os
import shutil
from math import modf

samplerate = 44100
framerate = 30

record_time = 120
warmup_time = 2
split_time = 60

# disk which must remain free...in tenths of a percent...e.g. 100 = 10.0% free
freespace = 100

outdir="/home/pi/output"
old_outputs=['%s/%s'%(outdir,output) for output in sorted(os.listdir(outdir))]
recdir="%s/%d"%(outdir,time())
os.mkdir(recdir)

with PiCamera(clock_mode='reset') as camera:
        try:
            print("setting camera parameters")
            camera.resolution = (1640,1232)
            #camera.resolution = (1024,768)
            camera.framerate = 30
            camera.rotation = 180
            print("starting camera preview")
            camera.start_preview()

            print("connecting to headset...")
            with Popen(['bluetoothctl','-a'],stdin=PIPE,stdout=DEVNULL,stderr=DEVNULL) as btctl:
                print("sending connect command")
                btctl.communicate(input="connect 20:74:CF:1E:6F:40".encode('utf-8'))

            print("starting ffmpeg...")
            with Popen(['ffmpeg','-y','-f','s16le','-ar',str(samplerate),'-ss',str(warmup_time),'-i','-','-c','aac','-f','adts','-f','segment','-segment_time',str(split_time),'%s/%%03d.aac'%(recdir)], stdin=PIPE, stdout=DEVNULL, stderr=DEVNULL) as ffmpeg:
                #give the camera a moment to startup, and feed ffmpeg some data to get it to warmup
                print("sending 2 seconds of warmup sound to ffmpeg")
                ffmpeg.stdin.write(bytes(2 * samplerate * warmup_time))

                print("starting arecord...")
                with Popen(['arecord','-D','bluealsa:HCI=hci0,DEV=20:74:CF:1E:6F:40,PROFILE=sco','--format=S16_LE','--rate='+str(samplerate),'--file-type=raw'], stdin=DEVNULL, stdout=ffmpeg.stdin) as arecord:
                    #initialize the filename counter
                    i = 0

                    #NB: starting the recording doesn't take a significant amount of time
                    print("starting video recording...")
                    videofn='%s/%03d.h264'%(recdir,i)
                    camera.start_recording(videofn, format='h264')

                    # this loop *should* be set to stop at *some* point...for now, let's set a limit of "record_time"
                    #while now - start < record_time:
                    while True: 
                        camera.wait_recording(1-modf(time())[0])
                        camera.annotate_text = strftime('%Y.%m.%d %H:%M:%S %Z')
                        timestamp = camera.frame.timestamp 
                        if timestamp is None:
                            continue
                        j = timestamp // 1000000 // split_time
                        if j > i:
                            i = j
                            old_outputs.append(videofn)
                            videofn='%s/%03d.h264'%(recdir,i)
                            print("splitting, starting on "+videofn)
                            camera.split_recording(videofn, format='h264')

                            while old_outputs:
                                stats = os.statvfs(outdir)
                                if stats.f_bavail * 1000 / stats.f_blocks > freespace:
                                    break
                                rmfr=old_outputs.pop(0)
                                print("removing %s"%rmfr)
                                try:
                                    os.remove(rmfr)
                                except IsADirectoryError:
                                    shutil.rmtree(rmfr, ignore_errors=True, onerror=None)

                    print("stopping video recording...")
                    camera.stop_recording()
                    print("killing arecord")
                    arecord.kill()
        finally:
            print("stopping camera preview")    
            camera.stop_preview()
            exit(0)
